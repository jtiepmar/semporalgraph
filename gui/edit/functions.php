<?php

	$textBoxValue ="";
	$finalize_facts ="";
	$fakten ="";
	$catoptions = '';
	

if (!file_exists(".htaccess")) {
echo '<script>
alert("ACHTUNG ACHTUNG!!!\nSICHERHEITSRISIKO fehlende Prüfung serverseitig ausgeführter Eingaben.\n\nBitte installieren Sie eine .htaccess Datei für Passwortschutz im gui/edit Ordner. Achten Sie darauf, die zugehörige .htpasswd serverseitig in einem unzugänglichen Ordner abzulegen(bspw. /home/...). \n\nOhne diese Datei werden keine für die Dateneingabe nötigen PHP Funktionen ausgeführt.")
</script>';
} 
else 
{
    if(array_key_exists('addCat', $_GET)) {
        addCat();
    }
    else if(array_key_exists('addFactRange', $_GET)) {
        addFactRange();
    }
	else if(array_key_exists('deleteFact', $_GET)) {
        deleteFact();
    }
    else if(array_key_exists('deleteCat', $_GET)) {
        deleteCat();
    }
    else if(array_key_exists('addFactTime', $_GET)) {
        addFactTime();
    }
	else if(array_key_exists('finalize', $_GET)) {
        finalize();
    }
	else if(array_key_exists('changeDataset', $_GET)) {
        changeDataset();
    }

}

	
    function finalize() 
	{
		shell_exec('python3 finalize.py '.$_GET['datasetName']);
		header('location: index.php');
	}
	
	function changeDataset() 
	{
		shell_exec('python3 changeDataset.py '.$_GET['selectNetwork']);
		header('location: index.php');
	}

	function deleteCat() 
	{
		$newKategorien = "";
		$fh = fopen('categories.txt','r');
		$linecount = 1;
		while ($line = fgets($fh)) {
			$counter = 0;
			foreach ($_GET['cattext'] as $deleteNum){
				if ($linecount == $deleteNum)
				{
					$counter += 1;					
				}
			}
			if ($counter == 0){
				$newKategorien .= $line;
			}
			$linecount +=1;
		}
		fclose($fh);
		$myfile = fopen("categories.txt", "w") or die("Unable to open file!");
		fwrite($myfile, $newKategorien);
		fclose($myfile);
		header('location: index.php');
	}

    function addcat() 
	{
		$fh = fopen('categories.txt','r');
		while ($line = fgets($fh)) {
			$textBoxValue .= $line;
		}
		fclose($fh);
		$cat = $_GET['cattext'];
		if (strlen($cat)>0 and strpos($textBoxValue,$cat)==False)
		{
			$myfile = fopen("categories.txt", "a") or die("Unable to open file!");
			fwrite($myfile, $cat."\n");
			fclose($myfile);
		}
		header('location: index.php');
	}
	
	function deleteFact() 
	{
		$newFakten = "";
		$fh = fopen('facts.txt','r');
		$linecount = 1;
		/*$deleteNum = $_GET['deleteFactNumber'];*/

		while ($line = fgets($fh)) {
			$counter = 0;
			foreach ($_GET['deleteFactNumber'] as $deleteNum){
				if ($linecount == $deleteNum)
				{
					$counter += 1;					
				}
			}
			if ($counter == 0){
				$newFakten .= $line;
			}
			$linecount +=1;
		}

		fclose($fh);
		$myfile = fopen("facts.txt", "w") or die("Unable to open file!");
		fwrite($myfile, $newFakten);
		fclose($myfile);
		header('location: index.php');
	}

    function addFactRange() 
	{
		$fh = fopen('facts.txt','r');
		while ($line = fgets($fh)) {
			$fakten .= $line;
		}
		fclose($fh);
		$s = trim($_GET['subjekt']);
		$sc = trim($_GET['subjektCat']).":";
		$v = trim($_GET['verb']);
		$o = trim($_GET['objekt']);
		$oc = trim($_GET['objektCat']).":";
		$zv = trim($_GET['zeitvon']);
		$zb = trim($_GET['zeitbis']);
		$qs = trim($_GET['sicherheit']);
		$q = trim($_GET['quelle']);
		if (strcmp($oc, "*Zeichenkette*:")==0) 
		{
			$o = '"'.$o.'"';
			$oc = '';
		}
		if (strcmp($sc, "*Zeichenkette*:")==0) 
		{
			$s = '"'.$s.'"';
			$sc = '';
		}
		if (strlen($s)>0 and strlen($v)>0 and strlen($v)>0){
			$fakt = $sc.$s."\t".$v."\t".$oc.$o."\t".$zv."\t".$zb."\t".$qs."\t".$q;
			if (strpos($fakten,$fakt)==False)
			{
				$myfile = fopen("facts.txt", "a") or die("Unable to open file!");
				fwrite($myfile, $fakt."\n");
				fclose($myfile);
			}
		}
		header('location: index.php');
	}

    function addFactTime() 
	{
		$fh = fopen('facts.txt','r');
		while ($line = fgets($fh)) {
			$fakten .= $line;
		}
		fclose($fh);
		$s = $_GET['subjekt'];
		$sc = $_GET['subjektCat'].":";
		$v = $_GET['verb'];
		$o = $_GET['objekt'];
		$oc = $_GET['objektCat'].":";
		$zv = $_GET['zeitbis'];
		$zb = $_GET['zeitbis'];
		$qs = $_GET['sicherheit'];
		$q = $_GET['quelle'];
		if (strcmp($oc, "*Zeichenkette*:")==0) 
		{
			$o = '"'.$o.'"';
			$oc = '';
		}
		if (strcmp($sc, "*Zeichenkette*:")==0) 
		{
			$s = '"'.$s.'"';
			$sc = '';
		}
		if (strlen($s)>0 and strlen($v)>0 and strlen($v)>0){
			$fakt = $sc.$s."\t".$v."\t".$oc.$o."\t".$zv."\t".$zb."\t".$qs."\t".$q;
			if (strpos($fakten,$fakt)==False)
			{
				$myfile = fopen("facts.txt", "a") or die("Unable to open file!");
				fwrite($myfile, $fakt."\n");
				fclose($myfile);
			}
		}
		header('location: index.php');
	}


	function getRows($field)
	{
		if (count(explode("\n",$field))-1 > 20) {
			return 20;
		}
		else {
			return count(explode("\n",$field))-1;
		}
	}


	function getOptions($choice)
	{
		$rtn = "";
		if ($choice == 1){
			$fh = fopen('facts.txt','r');
			$linecount = 1;
			$linecount2 = "";
			while ($line = fgets($fh)) {
				$fakten .= $linecount.". ".$line;
				$linecount2 = $linecount.".";

				/*for ($i=4; $i > strlen(strval($linecount)); $i--) { 
					$linecount2 .= " .";
				}*/

				$linecount2 .= "\t";

				$rtn .=  '<option value='.$linecount.'>'.$linecount2.$line.'</option>.\n';
				$linecount +=1;
			}
			fclose($fh);
		}
		else {
				$fh = fopen('categories.txt','r');
				$count = 1;
				while ($line = fgets($fh)) {
					$textBoxValue .=  $line;
					$rtn .= '<option value='.$count.'>'.$line.'</option>.\n';
					$count += 1;
				}
				fclose($fh);
		}
		return $rtn;
	}

	$fh = fopen('categories.txt','r');
	while ($line = fgets($fh)) {
		$textBoxValue .=  $line;
		$catoptions .= '<option>'.$line.'</option>';
	}
	fclose($fh);
	$fh = fopen('facts.txt','r');
	$linecount = 1;
	while ($line = fgets($fh)) {
		$fakten .= $linecount.". ".$line;
		$linecount +=1;
	}
	fclose($fh);
?>