<!DOCTYPE html>
<meta charset="utf-8">
<html>

<head>
	
  <!-- Plotly.js -->
  <script src="../../lib/urlhandling.js"></script>
  <script src="../../lib/config.js"></script>
  <script src="../../_assets/gui.js"></script>
  <link rel="stylesheet" href="../../styles.css">
</head>

<body style="background-color:lightgray">

<script>
document.write(header())
</script>
	<?php 
	include 'functions.php';
	?>
</head>

<body>

<br><br><br>
<div style="text-align:center;">
<form method="get">
<script>
	document.write(dataSelection())
</script>
	<input type="submit" id = "changeDataset" name="changeDataset" class="button" value="Wähle Datensatz" />
</form>
</div>


<script>
	function reload()
	{
		document.getElementById("changeDataset").disabled = false;
		defaultdataset = document.getElementById("selectNetwork").value
	}
	document.getElementById("changeDataset").disabled = true; 
</script>

<h2>
Dateneingabe
</h2>
<p>
Das hier ist die Dateneingabe. Kategorien können ganz unten konfiguriert werden. Nach Möglichkeit bitte immer eine Kategorie setzen und nur im Ausnahmefall *Zeichenkette* auswählen. Die Kategorien steuern, welche Art von Verknüpfung in dem Infofenster rechts neben dem Netzwerk erscheinen. <br>
Bitte kurz fassen. Subjekt, Verb und Objekt sollten möglichst nur einzelne Worte oder camelCase-Phrasen sein (Nutzer gibtEin Daten).<br>
Entweder eine Zeitspanne oder einen Zeitpunkt eingeben. Bei Zeitpunkten werden <i>von</i> und <i>bis</i> technisch gesehen gleichgesetzt. 
</p>

	<div class="addFactRange" >
		<br>
		<form method="get">

			<table align="center">
				<thead>
					<th>Subjekt</th>
					<th>Verb</th>
					<th>Objekt</th>
					<th>Zeitspanne</th>
					<th>Quelle (Soweit sie vorhanden ist)</th>
					<th>Quellensicherheit</th>
					<th></th>
				</thead>
				<tbody>
					<tr>
						<td><input type="textfield" name="subjekt" /></td>
						<td><input type="textfield" name="verb" /></td>
						<td><input type="textfield" name="objekt" /></td>
						<td id="dateselect">
							<input maxlength="4" size="5" type="number" min="1000" max="9999" step="1" value="1950" name="zeitvon" /> bis 
							<input maxlength="4" size="5" type="number" min="1000" max="9999" step="1" value="1950" name="zeitbis" />

						</td>
						<td><input type="textfield"  size="100" name="quelle" /></td>
						<td><select name="sicherheit">
							<option>1.0</option><option>0.9</option><option>0.8</option><option>0.7</option><option>0.6</option><option>0.5</option>
							<option>0.4</option><option>0.3</option><option>0.2</option><option>0.1</option>
						</select>
						<td><input type="submit" name="addFactRange" class="button" value="Fakt hinzufügen" /></td>

					</tr>
					<tr>
						<td><select name="subjektCat"><?php echo "<option>*Zeichenkette*</option>".$catoptions; ?></select></td>
						<td></td>
						<td><select name="objektCat"><?php echo "<option>*Zeichenkette*</option>".$catoptions; ?></select></td>
						<td></td>
						<td></td>
					</tr>

				</tbody>
			</table>
		</form>
	</div>

<br>
Oder
<br>

	<div class="addFactTime" >
		<br>
		<form method="get">
			<table align="center">
				<thead>
					<th>Subjekt</th>
					<th>Verb</th>
					<th>Objekt</th>
					<th>Zeitpunkt</th>
					<th >Quelle (Soweit sie vorhanden ist)</th>
					<th>Quellensicherheit</th>
					<th></th>
				</thead>
				<tbody>
					<tr>
						<td><input type="textfield" name="subjekt" /></td>
						<td><input type="textfield" name="verb" /></td>
						<td><input type="textfield" name="objekt" /></td>
						<td id="dateselect">
							<input maxlength="4" size="5" type="number" min="1000" max="9999" step="1" value="1950" name="zeitbis" />
						</td>
						<td><input type="textfield"  size="100" name="quelle" /></td>
						<td><select name="sicherheit">
							<option>1.0</option><option>0.9</option><option>0.8</option><option>0.7</option><option>0.6</option><option>0.5</option>
							<option>0.4</option><option>0.3</option><option>0.2</option><option>0.1</option>
						</select>
						<td><input type="submit" name="addFactTime" class="button" value="Fakt hinzufügen" /></td>
					</tr>
					<tr>
						<td><select name="subjektCat"><?php echo "<option>*Zeichenkette*</option>".$catoptions; ?></select></td>
						<td></td>
						<td><select name="objektCat"><?php echo "<option>*Zeichenkette*</option>".$catoptions; ?></select></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>

<br><br>
<form method="get">
	<input type="textfield" name="datasetName" />
	<input type="submit" name="finalize" class="button" value="Datensatz Finalisieren" />
</form>

<h2>
Datenkorrektur
</h2>
<p>
	Hier können eingegebene Fakten gelöscht werden.
</p>

	<div class="addFactTime">
		<br>
		<form method="get">
			<select name="deleteFactNumber[]" size="<?php echo getRows($fakten) ?>" multiple>
				<?php echo getOptions(1) ?>
			</select>
			<br>
			<input type="submit" name="deleteFact" value="Fakt löschen" />
		</form>
		<br>
	</div>
	<br><br>
	
<h2>
Konfiguration der Kategorien
</h2>
<p>
	Hier können Kategorien konfiguriert werden. Entweder neu hinzufügen oder bestehende löschen. Die Kategorien erscheinen dann in den Auswahlboxen in der Dateneingabe.
</p>

	<div class="addFactTime" id="categories">
		<br>
		<form method="get">
			<select name="cattext[]" size="<?php echo getRows($textBoxValue) ?>" multiple>
				<?php echo getOptions(2) ?>
			</select>
			<br>
			<input type="submit" name="deleteCat" value="Kategorie löschen" />
		</form>
		<br>
		<form method="get">
			<input type="textfield" name="cattext" />
			<input type="submit" name="addCat" class="button" value="Kategorie hinzufügen" />
		</form>
		<br>
	</div>
	<br><br>	
</head>
</html>