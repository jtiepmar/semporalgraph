import sys
import os
import collections
import shutil 
from datetime import datetime

dsn = sys.argv[1]
fn = "datasets/"+dsn

if not os.path.exists("datasets"):
	os.mkdir("datasets")
if os.path.exists(fn):
	os.rename(fn,fn+" vs."+str(datetime.timestamp(datetime.now())))
os.mkdir(fn)


	#groupsubgroup = Dateiordner, row = Spaltennummer, name=Name des neuen Ordners
#Es wird pro verschiedenem Value in einer Spalte eine einzelne Datei angelegt und dann mit den passenden Zeilen beschrieben
def grouping (groupsubgroup, name):
	files = {}
	content = {}
	
	#Anlegen des neuen Ordners
	if not os.path.exists(groupsubgroup.replace(",","/")+name+"/"):
		os.mkdir(groupsubgroup.replace(",","/")+name+"/")
			
	#Einlesen der Dateien und...
	with open (groupsubgroup+"/all.txt","r",encoding="utf-8") as f:
		counter = 0
		#print("Split "+groupsubgroup+"/"+file+" "+name)
		header=next(f).replace(",","\t")
		headerarr = header.split("\t")
		row = -1
		i = 0
		for key in headerarr:
			if name == key.strip():
				row = i
			i+=1
			
		for line in f:
			#line = line.replace("/","_")
			counter+=1
#				print(str(counter))
			arr = line.split("\t")
			#...pro Spaltenwert einmal...
			rowvalue = arr[row].strip()
			#...entweder neue Datei anlegen oder in vorherige hineinschreiben 
			fn = groupsubgroup+name+"/"+rowvalue.replace("/","_").replace(" ","_").replace(":","_").replace('"',"_")+".txt"
			if not fn in content:
				content[fn] = header
			content[fn] = content[fn] + line
			#print("####\n"+content[fn])
				
		for key in content.keys():
			with open(key, "w", encoding="utf8") as f:
				f.write(content[key])



def merging (groupsubgroup, name1, name2):
	files = {}
	content = {}
	
	#Anlegen des neuen Ordners
	if not os.path.exists(groupsubgroup.replace(",","/")+name1+"_"+name2+"/"):
		os.mkdir(groupsubgroup.replace(",","/")+name1+"_"+name2+"/")
			
	#Einlesen der Dateien und...
	with open (groupsubgroup+"/all.txt","r",encoding="utf-8") as f:
		counter = 0
		#print("Merge "+groupsubgroup+"/"+file+" "+name1+"_"+name2)
		header=next(f).replace(",","\t")
		headerarr = header.split("\t")
		row1 = -1
		row2 = -1
		i = 0
		for key in headerarr:
			if name1 == key.strip():
				row1 = i
			if name2 == key.strip():
				row2 = i
			i+=1
			
		for line in f:
			#line = line.replace("/","_")
			counter+=1
#				print(str(counter))
			arr = line.split("\t")
			#...pro Spaltenwert einmal...
			rowvalue1 = arr[row1].strip()
			rowvalue2 = arr[row2].strip()
			#...entweder neue Datei anlegen oder in vorherige hineinschreiben 
			fn = groupsubgroup+name1+"_"+name2+"/"+rowvalue1.replace("/","_").replace(" ","_").replace(":","_").replace('"',"_")+".txt"
			if not fn in content:
				content[fn] = header
			content[fn] = content[fn] + line
			fn = groupsubgroup+name1+"_"+name2+"/"+rowvalue2.replace("/","_").replace(" ","_").replace(":","_").replace('"',"_")+".txt"
			if not fn in content:
				content[fn] = header
			content[fn] = content[fn] + line

				#print("####\n"+content[fn])
				
		for key in content.keys():
			with open(key, "w", encoding="utf8") as f:
				f.write(content[key])



def categories (groupsubgroup, name1, name2):
	files = {}
	content = {}
	
	#Anlegen des neuen Ordners
	if not os.path.exists(groupsubgroup.replace(",","/")+"category"):
		os.mkdir(groupsubgroup.replace(",","/")+"category")
			
	#Einlesen der Dateien und...
	with open (groupsubgroup+"/all.txt","r",encoding="utf-8") as f:
		counter = 0
		#print("Merge "+groupsubgroup+"/"+file+" "+name1+"_"+name2)
		header=next(f).replace(",","\t")
		headerarr = header.split("\t")
		row1 = -1
		row2 = -1
		i = 0
		for key in headerarr:
			if name1 == key.strip():
				row1 = i
			if name2 == key.strip():
				row2 = i
			i+=1
			
		for line in f:
			#line = line.replace("/","_")
			counter+=1
#				print(str(counter))
			arr = line.split("\t")
			#...pro Spaltenwert einmal...
			rowvalue1 = arr[row1].strip()
			rowvalue2 = arr[row2].strip()
			if('"' in rowvalue1):
				continue
			if('"' in rowvalue2):
				continue
			rowvalue1 = rowvalue1.split(":")[0]
			rowvalue2 = rowvalue2.split(":")[0]
			#...entweder neue Datei anlegen oder in vorherige hineinschreiben 
			fn = groupsubgroup+"category/"+rowvalue1.replace("/","_").replace(" ","_").replace(":","_").replace('"',"_")+".txt"
			if not fn in content:
				content[fn] = header
			content[fn] = content[fn] + line
			fn = groupsubgroup+"category/"+rowvalue2.replace("/","_").replace(" ","_").replace(":","_").replace('"',"_")+".txt"
			if not fn in content:
				content[fn] = header
			content[fn] = content[fn] + line

				#print("####\n"+content[fn])
		allcats = ""
		for key in content.keys():
			allcats += key.strip().split("/")[3].split(".")[0]+"\n"
			with open(key, "w", encoding="utf8") as f:
				f.write(content[key])
		with open (groupsubgroup+"/categories.txt","w",encoding="utf-8" ) as catf:
			catf.write(allcats)
	print(allcats)
def normalize(unnorm):
	while ("  " in unnorm):
		unnorm = unnorm.replace("  "," ")
	norm = unnorm.replace(" \t","\t").replace("\t ","\t")
	return norm	
	
text = "subject\trelation\tobject\tfrom\tuntil\tfact trust\tprovenance\n"
fn+="/"
with open ("facts.txt","r",encoding="utf8") as ifile:
	for line in ifile:
		line = normalize(line)
		text += line
with open (fn+"all.txt", "w", encoding="utf8") as ofile:
	ofile.write(text)
categories(fn,"subject","object")
merging(fn,"subject","object")
grouping(fn,"subject")
grouping(fn,"object")
grouping(fn,"relation")



dsarr = os.listdir("datasets")
fl = ""
for ds in dsarr:
	fl+=ds+"\n"
	if os.path.exists("../../data/"+ds):
		shutil.rmtree("../../data/"+ds)
	shutil.copytree("datasets/"+ds, "../../data/"+ds)

with open ("../../data/datasets.txt", "w", encoding="utf8") as f:
	f.write(fl)
