var diagramfont= 'Arial'
var diagramfontsize= 16
var diagramlegendfontsize= 10
var dataurl="../../data/"
var sep="\t"
if(defaultdataset == null){
	var defaultdataset = "Leo Baeck Institut"
}
document.title = "Semporalgraph Viewer"
minYear = 1886
maxYear = 1991

var labelcolors = {
    "neutral":"lightgray",
    "positive":"green",
    "negative":"red"
}

var getColor = function(getcolortmp){
	return stringToColour(getcolortmp)
}
var normalize = function(unnorm){
	return unnorm.replaceAll(" ","_").replaceAll("/","_")
}

	
	
var stringToColour = function(getcolortmp) {
  var hash = 0;
  for (var i = 0; i < getcolortmp.length; i++) {
    hash = getcolortmp.charCodeAt(i) + ((hash << 5) - hash);
  }
  var colour = '#';
  for (var i = 0; i < 3; i++) {
    var value = (hash >> (i * 8)) & 0xFF;
    colour += ('00' + value.toString(16)).substr(-2);
  }
  return colour;
}
