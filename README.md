# Semporalgraph 

Bevor es losgeht: Diese Software ist unfertig und die Dateneingabe mit Sicherheitslücken verbunden. Wenn Sie nicht wissen, wie man serverseitig Zugänge per Passwort einschränkt, sollten Sie mit der Benutzung dieser Software abwarten, bis entsprechende Sicherheitsmechanismen implementiert wurden.

# Was und Warum?

Diese Software implementiert eine niedrigschwellig anwendbare Datenstruktur, die temporale und semantische Netzwerkdaten (Graphdaten) miteinander kombiniert und dabei minimale technische Anforderungen stellt. Zielgruppe sind Forscher und Forscherinnen aus den Geistes- und Sozialwissenschaften, die sich insbesondere mit zeitlichen (historischen) Daten beschäftigen und diese vernetzt ablegen möchten. Im Unterschied zum Semantic Web wird dabei keinerlei Wert auf Interoperabilität oder ähnliches gelegt und stattdessen ein komplett freies Konzept verfolgt. Ob Daten interoperabel sind ergibt sich aus der konkreten Verwendung und man muss nicht die konkrete Verwendung der Interoperabilität unterordnen. Das macht die Anwendung sehr viel simpler und nahbarer aber entfernt auch die automatische Kompatibilität mit bestehenden (Semantic Web) Lösungen, die durch gemeinsame Vokabulare entsteht.

Etwas genauer und akademischer ist es in der Datei HAIT DIKUSA Semporalgraph.pdf beschrieben. 

# Installation

Im Serverordner (bspw. var/www/)

git clone https://bitbucket.org/jtiepmar/semporalgraph/

https://[server]:[port]/semporalgraph sollte jetzt die GUI mit einem kleinen Datensatz anzeigen. Nicht wundern, wenn an der Stelle einzelne Diagramme usw nicht funktionieren. Es ist eine Baustelle und wenn irgendein Diagramm korrekt angezeigt wird, funktionierte die Installation.

## Dateneingabe konfigurieren

Die Dateneingabe passiert im Ordner/Tab "semporalgraph/gui/edit". Dort werden serverseitige PHP Skripte ausgeführt, deswegen unbedingt eine .htaccess Datei mit Passwortschutz anlegen. Ohne diese machen die Skripte nichts und es werden keine Datenänderungen vorgenommen.


Der Ordner "semporalgraph/gui/edit/datasets" benötigt Schreibrechte 

chmod -R 777 semporalgraph/gui/edit/datasets


Der Ordner "semporalgraph/data" benötigt Schreibrechte 

chmod -R 777 semporalgraph/data


# Dateneingabe

Die Dateneingabe ist im Moment noch etwas improvisiert und umständlich, aber sie funktioniert schonmal und erfordert so gut wie kein technisches Grundwissen neben dem Verständnis von semantischen Netzwerkdaten an sich. 

## Grundsätzliches Vorgehen: 

* Unten gewünschte Kategorien anlegen. Eine Kategorie bezeichnet die Klasse oder den Typ einer Entität, also was vor dem Doppelpunkt steht bei "Ort:Leipzig". Wenn man Kategorien nicht wünscht, kann auch alles als *Zeichenkette* bezeichnet werden.
* Oben die Fakten eingeben. Die vorgegebenen Kategorien können dabei über die Auswahlboxen für Subjekt und Objekt ausgewählt werden. Als Verb kann eine beliebige Zeichenkette eingegeben werden. Es macht natürlich Sinn, möglichst wiederkehrende und aussagekräftige Verben zu verwenden. camelCase-Schriftweise hilft bei der Lesbarkeit.
* Zeit und Quellensicherheit sind optional, werden aber in den Diagrammen beachtet. Wer diese Informationen weglässt, reduziert also die Aussagekraft der Visualisierungen.
* Nachdem alle Fakten eingegeben werden, einen Namen wählen und den Button "Datensatz finalisieren" drücken. Damit wird ein Skript ausgeführt (finalize.py), welches die Daten indexiert und in den gui/datasets Ordner verschiebt. Damit sind die Daten für die Diagramme in der Haupt-GUI zugänglich. 
* Sollte sich ein Name für den finalen Datensatz doppeln, wird über den Timestamp versioniert.

# Mehrnutzer und verschiedene Datensätze

Mehr als 1 editierender Nutzer pro Installation ist nicht empfehlenswert. Mehrere Datensätze pro Installation funktionieren aber es ist eher so gedacht, dass pro Installation 1 Datensatz gepflegt wird.

# Lizenz

CC BY https://creativecommons.org/licenses/by/4.0/legalcode


# Zitation

Tiepmar, Jochen (2023): Semporalgraph. URL: https://bitbucket.org/jtiepmar/semporalgraph/ .

# Entwicklungskontext

Diese Software wurde teilweise am Hannah-Arendt-Institut für Totalitarismusforschung e. V. (HAIT) der TU Dresden im Rahmen des DIKUSA Projektes entwickelt. Diese Maßnahme wird mitfinanziert mit Steuermittel auf Grundlage des vom Sächsischen Landtag beschlossenen Haushalt.