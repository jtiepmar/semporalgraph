function diagrammlink(parent)
{
	window.open(document.getElementById(parent).src, "_blank");
}

function printlink(parent)
{
	link = document.getElementById(parent).src
	if(link.includes("&print=1")){link=link.replace("&print=1","")}else{link+="&print=1"}
	document.getElementById(parent).src=link
}


function datalink(parent)
{
	window.open(dataurl+document.getElementById(parent).src.split("data=")[1].split("&")[0].replaceAll("_"," ")+".txt", "_blank");
}

function dataSelection(){
	dsarr = readTextFile("datasets").split("\n")
	dsstr=""
	for (ds in dsarr){
		dsstr+='<option value="'+dsarr[ds].replaceAll(" ","_")+'"'
		if(dsarr[ds]===defaultdataset){dsstr+=' selected="selected"'}
		dsstr+='>'+dsarr[ds]+'</option>'
		}
	str=""+
	'<select name="selectNetwork" id = "selectNetwork" onchange="reload()">'+
	dsstr+
	'</select>'
	return str
}

function header(){
	
str='<div  class="header">'+
'<table>'
+'<tr>'
+'<td>'
+'<a style="color:white;"  href="../home/">Netzwerk</a>'
+'</td>'
+'<td>'
+'<a style="color:white;"  href="../stats/">Statistiken</a>'
+'</td>'
+'<td>'
+'Elemente'
+'</td>'
+'<td>'
+'<a style="color:white;"  href="../export/">Export</a>'
+'</td>'
+'<td>'
+'<a style="color:white;"  href="../geomap/">Geomap</a>'
+'</td>'
+'<td>'
+'<a style="color:white;"  href="../edit/">Editieren</a>'
+'</td>'
+'</tr>'
+'<tr>'

+'<td style="text-align:center;">'
+'</td>'

+'<td style="text-align:center;">'
+'</td>'

+'<td style="text-align:center;">'
+'<a style="color:white;"  href="../entities/">Entitäten</a> | '
+'<a style="color:white;"  href="../relations/">Relationen</a>'

+'</td>'

+'<td style="text-align:center;">'
+'</td>'
+'<td style="text-align:center;">'
+'</td>'
+'<td style="text-align:center;">'
+'</td>'

+'</tr>'

+'</table>' 
+'</div>'
return str
}

function footer(){
str='<div class="header">'+
'<table>'
+'<tr>'


+'<td>'
+'<a href="https://digilab-hait.de/dikusa/">Projektseite</a>'
+'</td>'
+'<td>'
+'<a href="https://digilab-hait.de/hub/">HAIT Digilab</a>'
+'</td>'
+'</tr>'
+'</table>' 
+'</div>'
return str
}
